import multiprocessing
import pandas as pd
import zipfile

from collections import defaultdict


suspicious_entities = defaultdict(int)   # defaultdict is the most suitable for counting different items


def find_suspicious_transactions_by_date(date):
    """
    Finds suspicious transactions within provided date
    :param date: timestamp
    :return: a list with suspicious transactions
        in format [sending transaction ID, receiving transaction ID, date, amount received, amount sent,
                    transaction initializer ID, potential bridge ID, final recepient ID] or None
    """
    # get a Pandas DataFrame with all transactions for provided date
    # example: DataFrame[DataFrame.ColumnName == some_value]
    date_dataframe = data_frame[data_frame['TIMESTAMP'] == date]

    # get a list with potential bridges
    # the potential bridge is an entity which both received and sent money within a provided date
    # we use python set built-in method - intersection - to find out entities presented both
    # in SENDER and RECEIVER columns of a DataFrame
    potential_bridges_list = list(set(date_dataframe['SENDER']).intersection(set(date_dataframe['RECEIVER'])))

    # now iterate over potential bridges to find the bridge with ML conditions
    for bridge in potential_bridges_list:
        # get a Pandas DataFrames with all transactions where potential bridge is receiver and sender
        receiver_dataframe = date_dataframe[date_dataframe['RECEIVER'] == bridge]
        sender_dataframe = date_dataframe[date_dataframe['SENDER'] == bridge]

        for receiver_transaction in receiver_dataframe.itertuples():
            transaction_initializer = receiver_transaction.SENDER
            amount_received = receiver_transaction.AMOUNT

            for sender_transaction in sender_dataframe.itertuples():
                transaction_final_receiver = sender_transaction.RECEIVER
                amount_sent = sender_transaction.AMOUNT

                # now ML conditions below

                # if transaction initializer and final recipient are the same entity probably it's not a bridge
                sender_is_receiver = transaction_initializer == transaction_final_receiver

                # the same is for cases when entity person sends/receives money to/from itself
                to_itself = transaction_initializer == bridge
                from_itself = transaction_final_receiver == bridge

                # it must be a positive difference between amount received by bridge and sent further
                transactions_positive_difference_presence = amount_received - amount_sent > 0

                # the difference rate must be in range defined by user
                transactions_rate_is_suspicious = (amount_received - amount_sent) / amount_received <= rate

                if not sender_is_receiver and not to_itself and not from_itself \
                        and transactions_positive_difference_presence and transactions_rate_is_suspicious:
                    return [receiver_transaction.TRANSACTION, sender_transaction.TRANSACTION, date, amount_received,
                            amount_sent, transaction_initializer, bridge, transaction_final_receiver]


def write_potential_bridge_transactions_to_csv(bridges_list):
    """
    Writes potential bridge transactions to CSV file
    :param bridges_list: a list with potential bridges info returned by find_suspicious_transactions_by_date function
    :return: None, just creates or updates the file
    """
    potential_bridge_transactions = pd.DataFrame(
        bridges_list,
        columns=[
            'Sending Transaction ID',
            'Receiving Transaction ID',
            'Transaction Date',
            'Amount Received',
            'Amount Sent',
            'Initial Sender ID',
            'Potential Bridge ID',
            'Final Receiver ID',
        ]
    )
    potential_bridge_transactions.to_csv('suspicious_transactions.csv', sep='|', index=False)


def write_suspicious_entities_to_csv(entities_dict):
    """
    Writes suspicious entities to CSV file in descending order by transactions number
    :param entities_dict: a dictionary with suspicious entities as keys and number of
        their suspicious transactions as values
    :return: None, just creates or updates the file
    """
    data_sorted_by_desc_transactions_number = sorted(entities_dict.items(), key=lambda x: x[1], reverse=True)
    entities = pd.DataFrame(data_sorted_by_desc_transactions_number, columns=[
        'Suspicious Entity ID',
        'Number of Suspicious Transactions',
    ])
    entities.to_csv('suspicious_entities.csv', sep='|', index=False)


if __name__ == '__main__':
    # get the data from zip archive
    zip_file = zipfile.ZipFile('transactions.zip')
    data_frame = pd.read_csv(
        zip_file.open('transactions.csv'),
        delimiter='|',
        parse_dates=['TIMESTAMP'],
    )
    zip_file.close()

    # get unique dates from DataFrame
    dates = sorted(list(set(data_frame['TIMESTAMP'])))

    rate = 0.1

    potential_bridges = []
    pool = multiprocessing.Pool(processes=4)
    res = pool.map_async(find_suspicious_transactions_by_date, dates, callback=potential_bridges.append)
    pool.close()
    pool.join()
    potential_bridges = [x for x in potential_bridges[0] if x]   # get rid of empty values

    for bridge in potential_bridges:
        suspicious_entities[bridge[6]] += 1
    write_potential_bridge_transactions_to_csv(potential_bridges)
    write_suspicious_entities_to_csv(suspicious_entities)
